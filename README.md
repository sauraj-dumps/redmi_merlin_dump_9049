## merlin-user 10 QP1A.190711.020 V12.0.8.0.QJOMIXM release-keys
- Manufacturer: xiaomi
- Platform: mt6768
- Codename: merlin
- Brand: Redmi
- Flavor: merlin-user
merlin-user
merlinnfc-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: V12.0.8.0.QJOMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/merlin_global/merlin:10/QP1A.190711.020/V12.0.8.0.QJOMIXM:user/release-keys
- OTA version: 
- Branch: merlin-user-10-QP1A.190711.020-V12.0.8.0.QJOMIXM-release-keys
- Repo: redmi_merlin_dump_9049


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
